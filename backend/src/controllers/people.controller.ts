import {
    JsonController,
    Get,
    HttpCode,
    NotFoundError,
    Param, QueryParam
} from 'routing-controllers';
import { PeopleProcessing } from '../services/people_processing.service';

const peopleProcessing = new PeopleProcessing();

@JsonController('/people', { transformResponse: false })
export default class PeopleController {
    @HttpCode(200)
    @Get('/all')
    getAllPeople(@QueryParam("limit") limit?: number, @QueryParam("page") page?: number) {
        const { data, totalPages } = peopleProcessing.getAll(page || 1, limit || 20);

        if (!data) {
            throw new NotFoundError('No people found');
        }

        return {
            data,
            limit: limit || 20,
            page: page || 1,
            totalPages
        };
    }

    @HttpCode(200)
    @Get('/:id')
    getPerson(@Param('id') id: number) {
        const person = peopleProcessing.getById(id);

        if (!person) {
            throw new NotFoundError('No person found');
        }

        return {
            data: person,
        };
    }
}
