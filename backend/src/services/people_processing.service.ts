import people_data from '../data/people_data.json';

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    getAll(page: number, limit: number) {
      const pages = [];
      for (let i = 0; i < people_data.length; i += limit) {
          const chunk = people_data.slice(i, i + limit);
          pages.push(chunk);
      }

      return { data: pages[page - 1], totalPages: pages.length };
    }
}
