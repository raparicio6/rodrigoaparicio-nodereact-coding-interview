import axios from "axios";
import { IUserProps } from "../dtos/user.dto";

interface GetAllUsersResponse {
  data: IUserProps[];
  limit: number;
  page: number;
  totalPages: number;
}

export class BackendClient {
  private readonly baseUrl: string;

  constructor(baseUrl = "http://localhost:3001/v1") {
    this.baseUrl = baseUrl;
  }

  async getAllUsers(page?: number, limit?: number): Promise<GetAllUsersResponse> {
    return (await axios.get(`${this.baseUrl}/people/all`, {params: { page, limit }})).data;
  }
}
