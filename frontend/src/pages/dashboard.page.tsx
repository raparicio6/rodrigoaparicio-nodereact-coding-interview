import React, { FC, useState, useEffect, useCallback } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { Filters } from "../components/filters/filters";
import { CircularProgress, Pagination } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [title, setTitle] = useState('');
  const [gender, setGender] = useState('');
  const [company, setCompany] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      const result = await backendClient.getAllUsers(page);
      setUsers(result.data);
      setTotalPages(result.totalPages);
      setLoading(false);
    };

    fetchData();
  }, [page, title, gender]);

  const handlePaginationOnChange = useCallback((_, newPage: number) => {
    setPage(newPage);
  }, []);

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            <Filters title={} />
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null} 
              <Pagination count={totalPages} defaultPage={1} page={page} onChange={handlePaginationOnChange} />
          </div>
        )}
      </div>
    </div>
  );
};
